`timescale 1ns / 1ns
`default_nettype wire

//    This file is part of the ZXUNO Spectrum core. 
//    Creation date is 02:28:18 2014-02-06 by Miguel Angel Rodriguez Jodar
//    (c)2014-2020 ZXUNO association.
//    ZXUNO official repository: http://svn.zxuno.com/svn/zxuno
//    Username: guest   Password: zxuno
//    Github repository for this core: https://github.com/mcleod-ideafix/zxuno_spectrum_core
//
//    ZXUNO Spectrum core is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ZXUNO Spectrum core is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//e
//    You should have received a copy of the GNU General Public License
//    along with the ZXUNO Spectrum core.  If not, see <https://www.gnu.org/licenses/>.
//
//    Any distributed copy of this file must keep this notice intact.

//Max
//UnoXT Generation Selector
//`define unoxt
`define unoxt2

`ifdef unoxt
	module unoxt_top (
`elsif unoxt2
	module unoxt2_top (
`else
	module unoxt_top (
`endif

   input wire clock_50_i,
	
   output wire [4:0] rgb_r_o,
   output wire [4:0] rgb_g_o,
   output wire [4:0] rgb_b_o,
   output wire hsync_o,
   output wire vsync_o,
   input wire ear_port_i,
   output wire mic_port_o,
   inout wire ps2_kbd_clk_io,
   inout wire ps2_kbd_data_io,
   inout wire ps2_mouse_clk_io,
   inout wire ps2_mouse_data_io,
   output wire audioext_l_o,
   output wire audioext_r_o,

   inout wire esp_gpio0_io,
   inout wire esp_gpio2_io,
	output wire esp_tx_o,
   input wire esp_rx_i,
 
   output wire [20:0] ram_addr_o,
	output wire ram_lb_n_o,
	output wire ram_ub_n_o,
   inout wire [15:0] ram_data_io,
   output wire ram_oe_n_o,
   output wire ram_we_n_o,
   output wire ram_ce_n_o,
   
   output wire flash_cs_n_o,
   output wire flash_sclk_o,
   output wire flash_mosi_o,
   input wire flash_miso_i,
	output wire flash_wp_o,
	output wire flash_hold_o,
   
   input wire joyp1_i,
   input wire joyp2_i,
   input wire joyp3_i,
   input wire joyp4_i,
   input wire joyp6_i,
   output wire joyp7_o,
   input wire joyp9_i,
	
   input wire btn_divmmc_n_i,
   input wire btn_multiface_n_i,

   output wire sd_cs0_n_o,    
   output wire sd_sclk_o,     
   output wire sd_mosi_o,    
   input wire sd_miso_i,

`ifdef unoxt2
	
   output wire [12:0] sdram_addr,
   output wire [1:0] sdram_ba,
	output wire sdram_cke,
 	output wire sdram_we_n,
 	output wire sdram_cas_n,
 	output wire sdram_ras_n,
 	output wire sdram_cs_n,
   inout wire [15:0] sdram_data,
 	output wire sdram_ldqm,
 	output wire sdram_udqm,
 	output wire sdram_clk,

`endif

	output wire led_red_o,
	output wire led_yellow_o,
   output wire led_green_o,   // nos servir como testigo de uso de la SPI
	output wire led_blue_o

	//output wire [8:0] test

   );
   
   //Max
	`define DEFAULT_100_LEDS;
	
	`ifdef ROUND_DIF_LEDS
		//3mm round diffused LEDs
		localparam ledRedK = 16'd20;
		localparam ledYellowK = 16'd50;
		localparam ledGreenK = 16'd12;
		localparam ledBlueK = 16'd50;
	`elsif SQUARE_LEDS
		//square color LEDs
		localparam ledRedK = 16'd20;
		localparam ledYellowK = 16'd33;
		localparam ledGreenK = 16'd100;
		localparam ledBlueK = 16'd20;
	`else
		//default 100% LEDs
		localparam ledRedK = 16'd100;
		localparam ledYellowK = 16'd100;
		localparam ledGreenK = 16'd100;
		localparam ledBlueK = 16'd100;
	`endif
	
	wire sysclk;
	wire greenLed;
	wire [5:0] rgb_r, rgb_g, rgb_b;
	
	system_2MB sys_inst
	(
		.CLK_50MHZ(clock_50_i),
		.sysclk(sysclk),
		.VGA_R(rgb_r),
		.VGA_G(rgb_g),
		.VGA_B(rgb_b),
		.VGA_HSYNC(hsync_o),
		.VGA_VSYNC(vsync_o),
		.SRAM_ADDR(ram_addr_o),
		.SRAM_DATA(ram_data_io[7:0]),
		.SRAM_WE_n(ram_we_n_o),
		.LED(),
		.SD_n_CS(sd_cs0_n_o),
		.SD_DI(sd_mosi_o),
		.SD_CK(sd_sclk_o),
		.SD_DO(sd_miso_i),
		.AUD_L(audioext_l_o),
		.AUD_R(audioext_r_o),
	 	.PS2_CLK1(ps2_kbd_clk_io),
		.PS2_CLK2(ps2_mouse_clk_io),
		.PS2_DATA1(ps2_kbd_data_io),
		.PS2_DATA2(ps2_mouse_data_io)
	);
	
	assign rgb_r_o = rgb_r[5:1];
	assign rgb_g_o = rgb_g[5:1];
	assign rgb_b_o = rgb_b[5:1];

 	assign joyp7_o = 1'b1;
	
	assign ram_oe_n_o = 1'b0;
	assign ram_ce_n_o = 1'b0;
	assign ram_lb_n_o = 1'b0;
	assign ram_ub_n_o = 1'b1;
	assign ram_data_io[15:8] = 8'bz;
	
	assign mic_port_o = 1'b1;
 
	assign esp_gpio0_io = 1'b1;
   assign esp_gpio2_io = 1'b1;
	assign esp_tx_o = 1'b1;
	
	assign flash_cs_n_o = 1'b1;
	assign flash_sclk_o = 1'b1;
	assign flash_mosi_o = 1'b1;
	assign flash_wp_o = 1'b1;
	assign flash_hold_o = 1'b1;
	
	assign greenLed = !sd_cs0_n_o;
	/*flashCnt #(.CLK_MHZ(16'd21)) cnt(
		.clk(sysclk),
		.signal(sd_sclk_o),
		.msec(16'd20),
		.flash(greenLed)
	);*/
	
`ifdef unoxt2
	
   assign sdram_addr = 13'b0;
   assign sdram_ba = 2'b1;
	assign sdram_cke = 1'b0;
 	assign sdram_we_n = 2'b1;
 	assign sdram_cas_n = 2'b1;
 	assign sdram_ras_n = 2'b1;
 	assign sdram_cs_n = 2'b1;
   assign sdram_data = 16'bz;
 	assign sdram_ldqm = 2'b1;
 	assign sdram_udqm = 2'b1;
 	assign sdram_clk= 2'b1;

`endif

	
 	ledPWM ledRed(
		.nReset(1'b1),
		.clock(sysclk),
		.enable(1'b1),
		.y1(16'd100),
		.y2(ledRedK),	
		.led(led_red_o)
    );

	ledPWM ledYellow(
		.nReset(1'b1),
		.clock(sysclk),
		.enable(1'b0),
		.y1(16'd100),
		.y2(ledYellowK),	
		.led(led_yellow_o)
    );

	ledPWM ledGreen(
		.nReset(1'b1),
		.clock(sysclk),
		.enable(greenLed),
		.y1(16'd100),
		.y2(ledGreenK),	
		.led(led_green_o)
    );

	ledPWM ledBlue(
		.nReset(1'b1),
		.clock(sysclk),
		.enable(1'b0),
		.y1(16'd100),
		.y2(ledBlueK),	
		.led(led_blue_o)
    );
  
endmodule
