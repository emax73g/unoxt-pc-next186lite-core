@echo off

    if exist "BIOS.obj" del "BIOS.obj"
    if exist "BIOS.com" del "BIOS.com"

    c:\masm611\bin\ml /AT /c /Fl BIOS.asm
    if errorlevel 1 goto errasm

    c:\masm611\binr\link /TINY BIOS.obj,BIOS.com,,,,
    if errorlevel 1 goto errlink
    rem dir "BIOS.*"
    goto TheEnd

  :errlink
    echo _
    echo Link error
    goto TheEnd

  :errasm
    echo _
    echo Assembly Error
    goto TheEnd
    
  :TheEnd

pause